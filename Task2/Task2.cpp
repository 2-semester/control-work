#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct Map
{
	char symbol;
	int number;
	static const int count = 26;
};

Map *createMap();
void writeMapToFile(string, Map *);
void displayContentFileWithMap(string);
void encoding(string, string, Map *);
void decoding(string, string, Map const *);
Map *createDictionary(string);
int encoding(char, Map *);
char decoding(int, Map const *);

int main()
{
	Map *map = createMap();
	writeMapToFile("codes", map);
	displayContentFileWithMap("codes");
	encoding("input.txt", "result.txt", map);
	decoding("result.txt", "output.txt", map);
}

Map *createMap()
{
	Map *mapArray = new Map[Map::count];

	char symbol = 'A';
	int number = Map::count;

	for (int i = 0; i < Map::count; i++)
	{
		mapArray[i].symbol = symbol;
		mapArray[i].number = number;

		symbol++;
		number--;
	}

	return mapArray;
}

void writeMapToFile(string fileName, Map *map)
{
	ofstream fout(fileName, ios::binary);

	if (!fout.is_open())
	{
		cout << "Unable to open the file." << endl;
		return;
	}

	fout.write((char *)map, sizeof(Map) * Map::count);
	fout.close();
}

void displayContentFileWithMap(string fileName)
{
	ifstream fin(fileName, ios::binary);

	Map *map = new Map[Map::count];
	fin.read((char *)map, sizeof(Map) * Map::count);

	for (int i = 0; i < Map::count; i++)
	{
		cout << map[i].symbol << ": " << map[i].number << endl;
	}

	fin.close();
}

Map *createDictionary(string fileName)
{
	ifstream fin(fileName, ios::binary);

	Map *map = new Map[Map::count];
	fin.read((char *)map, sizeof(Map) * Map::count);
	fin.close();
	return map;
}

int encoding(char symbol, Map *map)
{
	symbol = toupper(symbol);

	for (int i = 0; i < Map::count; i++)
	{
		if (map[i].symbol == symbol)
		{
			return map[i].number;
		}
	}

	return (int)symbol;
}

void encoding(string input, string output, Map *map)
{
	ifstream fin(input);
	if (!fin)
	{
		cout << "Failed to open input file." << endl;
		return;
	}

	ofstream fout(output);
	if (!fout)
	{
		cout << "Failed to open output file." << endl;
		return;
	}

	string line;
	while (getline(fin, line))
	{
		for (int i = 0; i < line.length(); i++)
		{
			int symbolId = encoding((char)line[i], map);
			fout << symbolId << " ";
		}
		fout << endl;
	}

	fin.close();
	fout.close();
}

void decoding(string input, string output, Map const *map)
{
	ifstream fin(input);
	if (!fin)
	{
		cout << "Failed to open input file." << endl;
		return;
	}

	ofstream fout(output);
	if (!fout)
	{
		cout << "Failed to open output file." << endl;
		return;
	}

	string line;
	while (getline(fin, line))
	{
		int start = -1;
		for (int i = 0; i < line.length(); i++)
		{
			if (line[i] != ' ')
			{
				if (start == -1)
				{
					start = i;
				}
			}
			else if (start != -1)
			{
				string numberCode = line.substr(start, i - start);
				fout << decoding(stoi(numberCode), map);
				start = -1;
			}
		}

		if (start != -1)
		{
			string numberCode = line.substr(start);
			fout << decoding(stoi(numberCode), map);
		}

		fout << endl;
	}

	fin.close();
	fout.close();
}

char decoding(int item, Map const *map)
{
	for (int i = 0; i < Map::count; i++)
	{
		if (map[i].number == item)
		{
			return map[i].symbol;
		}
	}

	return (char)item;
}