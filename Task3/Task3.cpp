#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);

int main()
{
	calculateSumTest("test.txt");
}

int calculateSum(string source)
{
	int decimalSum = 0;
    int start = -1;

    for (int i = 0; i < source.length(); i++) {
        if (source[i] == '0' || source[i] == '1') {
            if (start == -1) {
                start = i;
            }
        }
        else if (start != -1) {
            string binaryNumber = source.substr(start, i - start);
            decimalSum += binaryToDecimal(binaryNumber);
            start = -1;
        }
    }

    if (start != -1) {
        string binaryNumber = source.substr(start);
        int decimalNumber = binaryToDecimal(binaryNumber);
        decimalSum += decimalNumber;
    }

    return decimalSum;
}

int calculateSumFromFile(string fileName)
{
	ifstream fin(fileName);

	int result = 0;
	string line;
	while (getline(fin, line))
	{
		result += calculateSum(line);
	}

	fin.close();
	return result;
}

int binaryToDecimal(string binary)
{
	return stoi(binary, nullptr, 2);
}

void calculateSumTest(string fileName)
{
	bool result = calculateSum("1+-0100+*** 1000") == 13;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSumFromFile(fileName) == 4262515;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}